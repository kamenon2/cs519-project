precision mediump float;
varying vec2 fragTextCoord;
varying vec3 fragmentNormal;
uniform sampler2D sampler;

void main()
{
    vec3 ambientLight = vec3 (0.5, 0.5, 0.3); //eg: bright day - high ambient light. This just represents how much light is permeating the scene.
    vec3 diffuseLight = vec3 (0.6, 0.6, 0.3); // intensity of the sunlight
    vec3 specularLight = normalize (vec3 (2.0, -4.0, -5.0)); // directional light and the brightness from that direction

    //ambient and diffuse are the same color
    vec4 texelColor = texture2D (sampler, fragTextCoord);
    //ambient + diffuse + specular
    vec3 lightIntensity = ambientLight + (diffuseLight * max(dot(fragmentNormal, specularLight), 0.0));

    gl_FragColor = vec4(texelColor.rgb * lightIntensity, texelColor.a);

    //gl_FragColor = vec4(fragmentNormal, 1.0);
    //gl_FragColor = texture2D(sampler, fragTextCoord);
}