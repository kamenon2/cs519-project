precision mediump float;

attribute vec3 vertexPosition;
attribute vec2 vertexTextCoord;
attribute vec3 vertexNormal;

varying vec2 fragTextCoord;
varying vec3 fragmentNormal;

uniform mat4 mWorld;
uniform mat4 mView;
uniform mat4 mProj;

void main()
{
    fragTextCoord = vertexTextCoord;

    //fragmentNormal = vertexNormal;
    fragmentNormal = (mWorld * vec4(vertexNormal, 0.0)).xyz; //this normally returns 4-dim, so we have used .xyz to just get the 3-dim


    gl_Position = mProj * mView * mWorld * vec4(vertexPosition, 1.0);
}