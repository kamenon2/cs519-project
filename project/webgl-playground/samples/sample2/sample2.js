var RotateTriangle = function () {
    console.log('Create a triangle initiated ...');
    console.log(Date.now().toString());

    var canvas = document.getElementById('sample-surface-2');
    var gl = canvas.getContext("webgl2");

    if (!gl){
        alert('Your browser does not support WebGL');
    }

    /*
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    gl.viewport(0, 0, window.innerWidth, innerHeight)
    */
    gl.clearColor(R = 0.75, G = 0.85, B = 0.8, A = 0.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT); // to paint. need to set the color buffer and depth buffer. eg: CIRCLE BEHIND SQUARE

    //Vertex shader and Fragment shader
    var vertexShader = gl.createShader(gl.VERTEX_SHADER);
    var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(vertexShader, vertexShaderText);
    gl.shaderSource(fragmentShader, fragmentShaderText);
    gl.compileShader(vertexShader);
    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
        console.error('ERROR compiling vertex shader!', gl.getShaderInfoLog(vertexShader));
        return;
    }
    console.log(vertexShader);

    gl.compileShader(fragmentShader);
    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
        console.error('ERROR compiling vertex shader!', gl.getShaderInfoLog(fragmentShader));
        return;
    }
    console.log(fragmentShader);

    var prgrm = gl.createProgram();
    gl.attachShader(prgrm, vertexShader);
    gl.attachShader(prgrm, fragmentShader);

    //link the program
    gl.linkProgram(prgrm);
    if (!gl.getProgramParameter(prgrm, gl.LINK_STATUS)) {
        console.error('ERROR linking program!', gl.getProgramInfoLog(prgrm));
        return;
    }
    gl.validateProgram(prgrm);
    if(!gl.getProgramParameter(prgrm, gl.VALIDATE_STATUS)) {
        console.error('ERROR validation program!', gl.getProgramInfoLog(prgrm))
        return;
    }

    /*
        - Vertices are the points that define the corners of 3D objects.
        - Each vertex is represented by three floating-point numbers that correspond to the x,y and x coordinates of the vertex
        - these vertices are represents in Javascript array
     */
    var triangleVertices =
        [
            //X,    Y,     Z values     R,   G,   B values
            0.1,   0.5,   0.4,          1.0, 1.0, 0.0,
            -0.2,  -0.8,  -0.3,          0.9, 0.0, 1.0,
            0.9,   -0.2,  -0.2,          0.1, 1.0, 0.6

        ];

    var triangleVertexBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, triangleVertexBufferObject);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVertices), gl.STATIC_DRAW);// CPU TO GPU MEMORY

    var positionAttributeLocation = gl.getAttribLocation(prgrm, 'vertexPosition');
    var colorAttributeLocation = gl.getAttribLocation(prgrm, 'vertexColor');

    gl.vertexAttribPointer(positionAttributeLocation,
        3,
        gl.FLOAT,
        false,
        6 * Float32Array.BYTES_PER_ELEMENT,
        0
    );

    gl.vertexAttribPointer(colorAttributeLocation,
        3,
        gl.FLOAT,
        false,
        6 * Float32Array.BYTES_PER_ELEMENT,
        3 * Float32Array.BYTES_PER_ELEMENT
    );

    gl.useProgram(prgrm);
    gl.enableVertexAttribArray(positionAttributeLocation);
    gl.enableVertexAttribArray(colorAttributeLocation);

    //location of the spaces in the GPU.
    var mWorldUniLoc = gl.getUniformLocation(prgrm, 'mWorld');
    var mViewUniLoc = gl.getUniformLocation(prgrm, 'mView');
    var mProjUniLoc = gl.getUniformLocation(prgrm, 'mProj');

    var worldMatrix = new Float32Array(16);
    var viewMatrix = new Float32Array(16);
    var projMatrix = new Float32Array(16);
    // and this is how the 3 arrays above get converted to 3 identities below.
    // .. the input gets converted automatically.
    mat4.identity(worldMatrix);
    mat4.lookAt(viewMatrix, [0, 0, -2], [0, 0, 0], [0, 1, 0]);
    mat4.perspective(projMatrix, glMatrix.toRadian(45), canvas.clientWidth / canvas.clientHeight, 0.1, 1000.0);

    //sending the matrices to the shader
    gl.uniformMatrix4fv(mWorldUniLoc, false, worldMatrix);
    gl.uniformMatrix4fv(mViewUniLoc, false, viewMatrix);
    gl.uniformMatrix4fv(mProjUniLoc, false, projMatrix);

    var angle = 0;
    var idMatrix = new Float32Array(16);
    mat4.identity(idMatrix);

    var loop = function () {
        angle = performance.now() / 1000 / 2 * 2 * Math.PI;

        mat4.rotate(worldMatrix, idMatrix, angle, [1, 1, 1]);

        gl.uniformMatrix4fv(mWorldUniLoc, false, worldMatrix);

        gl.clearColor(0.75, 0.85, 0.8, 0.0);

        gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

        gl.drawArrays(gl.TRIANGLES, 0, 3);

        requestAnimationFrame(loop);
    };

    requestAnimationFrame(loop);


};


//Vertex shader to fragment shader
var vertexShaderText =
    [
        'precision mediump float;',
        '',
        'attribute vec3 vertexPosition;',
        'attribute vec3 vertexColor;',
        'varying vec3 fragColor;',
        'uniform mat4 mWorld;',
        'uniform mat4 mView;',
        'uniform mat4 mProj;',
        '',
        'void main()',
        '{',
        ' fragColor = vertexColor;',
        ' gl_Position = mProj * mView * mWorld * vec4(vertexPosition, 1.0);',
        '}'
    ].join('\n');

var fragmentShaderText =
    [
        'precision mediump float;',
        '',
        'varying vec3 fragColor;',
        'void main()',
        '{',
        ' gl_FragColor = vec4(fragColor, 1.0);', // frag color has 3 colors = RED, GREEN AND BLUE
        '}'
    ].join('\n');

// This is just the regular way to right the functions instead of the text format we used above
function vertexShader (vertexPosition, vertexColor) {
    return {
        fragColor: vertexColor,
        gl_position: [vertexPosition.x, vertexPosition.y, 0.0, 1.0]
    }
}