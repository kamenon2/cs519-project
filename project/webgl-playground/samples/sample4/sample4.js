const utils = {

    getCanvas(elementId) {
        console.log(Date.now().toString());
        const canvas = document.getElementById(elementId);
        if (!canvas) {
            console.error('There is no canvas with id ${id} on this page.')
            alert('There is no canvas with id ${id} on this page.');
            return null;
        }
        return canvas;
    },

    getWebGlContext (canvas, contextId) {
        console.log(Date.now().toString());
        var gl = canvas.getContext(contextId);
        if (!gl){
            console.error('Your browser does not support WebGL');
            alert('Your browser does not support WebGL');
        }
        return gl;
    }
}

var loadPage = function () {
    loadText('shader.vs.glsl', function (vsErr, vsText) {
       if (vsErr) {
           alert('Failed vertex shader load');
           console.error(vsErr);
       } else {
           loadText('shader.fs.glsl', function (fsErr, fsText) {
               if (fsErr) {
                   alert('Failed fragment shader load');
                   console.error(fsErr);
               } else {
                   loadJson('blender/wooden crate.json', function (modelError, modelObject){
                        if (modelError) {
                            alert('Fatal error tryig to get model');
                            console.error(modelError);
                        } else {
                            loadImage('blender/terrain_atlas.png', function (imageError, image) {
                               if (imageError) {
                                   alert('Fatal error trying to get image');
                                   console.error(imageError);
                               } else {
                                   RotatingCrate(vsText, fsText, image, modelObject)
                               }
                            });

                        }
                   });

               }
           });
       }
    });
};

var RotatingCrate = function (vertexShaderText, fragmentShaderText, image, modelObject) {

    console.log(modelObject);
    console.log(image);
    console.log('Making a Rotating Crate initiated ... ');

    var canvas = utils.getCanvas("rotating-crate-surface");
    var gl = utils.getWebGlContext(canvas,"webgl2");

    gl.clearColor(0.75, 0.85, 0.8, 0.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.CULL_FACE);
    gl.frontFace(gl.CCW);
    gl.cullFace(gl.BACK);

    var vertexShader = gl.createShader(gl.VERTEX_SHADER);
    var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(vertexShader, vertexShaderText);
    gl.shaderSource(fragmentShader, fragmentShaderText);

    gl.compileShader(vertexShader);
    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
        console.error('ERROR compiling vertex shader!', gl.getShaderInfoLog(vertexShader));
        return;
    }

    gl.compileShader(fragmentShader);
    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
        console.error('ERROR compiling fragment shader!', gl.getShaderInfoLog(fragmentShader));
        return;
    }

    var prgrm = gl.createProgram();
    gl.attachShader(prgrm, vertexShader);
    gl.attachShader(prgrm, fragmentShader);
    gl.linkProgram(prgrm);

    if (!gl.getProgramParameter(prgrm, gl.LINK_STATUS)) {
        console.error('ERROR linking program!', gl.getProgramInfoLog(program));
        return;
    }

    gl.validateProgram(prgrm);
    if (!gl.getProgramParameter(prgrm, gl.VALIDATE_STATUS)) {
        console.error('ERROR validating program!', gl.getProgramInfoLog(program));
        return;
    }

    var boxVertices = modelObject.meshes[0].vertices;
    var boxIndices = [].concat.apply([], modelObject.meshes[0].faces);
    var boxTexCoords = modelObject.meshes[0].texturecoords[0];

    var boxPositionVertexBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, boxPositionVertexBufferObject);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(boxVertices), gl.STATIC_DRAW);

    var boxTexCoordVertexBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, boxTexCoordVertexBufferObject);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(boxTexCoords), gl.STATIC_DRAW);

    var boxIndexBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, boxIndexBufferObject);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(boxIndices), gl.STATIC_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER, boxPositionVertexBufferObject);
    var positionAttribLocation = gl.getAttribLocation(prgrm, 'vertexPosition');
    gl.vertexAttribPointer(positionAttribLocation,
        3,
        gl.FLOAT,
        false,
        3 * Float32Array.BYTES_PER_ELEMENT,
        0
    );
    gl.enableVertexAttribArray(positionAttribLocation);

    gl.bindBuffer(gl.ARRAY_BUFFER, boxTexCoordVertexBufferObject);
    var texCoordAttribLocation = gl.getAttribLocation(prgrm, 'vertexTextCoord');
    gl.vertexAttribPointer(
        texCoordAttribLocation, // Attribute location
        2, // Number of elements per attribute
        gl.FLOAT, // Type of elements
        false,
        2 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
        0
    );
    gl.enableVertexAttribArray(texCoordAttribLocation);

    //
    // Crate texture is the terrain atlas png file.
    //
    var boxTexture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, boxTexture);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texImage2D(
        gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA,
        gl.UNSIGNED_BYTE,
        image
    );

    gl.bindTexture(gl.TEXTURE_2D, null);
    gl.useProgram(prgrm);

    // and this is how the 3 arrays above get converted to 3 identities below.
    // .. the input gets converted automatically.
    var matWorldUniformLocation = gl.getUniformLocation(prgrm, 'mWorld');
    var matViewUniformLocation = gl.getUniformLocation(prgrm, 'mView');
    var matProjUniformLocation = gl.getUniformLocation(prgrm, 'mProj');

    var worldMatrix = new Float32Array(16);
    var viewMatrix = new Float32Array(16);
    var projMatrix = new Float32Array(16);
    mat4.identity(worldMatrix);
    mat4.lookAt(viewMatrix, [0, 0, -8], [0, 0, 0], [0, 1, 0]);
    mat4.perspective(projMatrix, glMatrix.toRadian(45), canvas.clientWidth / canvas.clientHeight, 0.1, 1000.0);

    gl.uniformMatrix4fv(matWorldUniformLocation, gl.FALSE, worldMatrix);
    gl.uniformMatrix4fv(matViewUniformLocation, gl.FALSE, viewMatrix);
    gl.uniformMatrix4fv(matProjUniformLocation, gl.FALSE, projMatrix);

    var xRotateMatrix = new Float32Array(16);
    var yRotateMatrix = new Float32Array(16);

    var idMatrix = new Float32Array(16);
    mat4.identity(idMatrix);

    var angle = 0;

    var loop = function () {
        angle = performance.now() / 1000 / 6 * 2 * Math.PI;

        mat4.rotate(xRotateMatrix, idMatrix, angle, [1, 0, 0]);
        mat4.rotate(yRotateMatrix, idMatrix, angle / 2, [0, 1, 0]);
        mat4.mul(worldMatrix, yRotateMatrix, xRotateMatrix);

        gl.uniformMatrix4fv(matWorldUniformLocation, false, worldMatrix);

        gl.clearColor(0.75, 0.85, 0.8, 1.0);
        gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

        gl.bindTexture(gl.TEXTURE_2D, boxTexture);
        gl.activeTexture(gl.TEXTURE0);

        gl.drawElements(gl.TRIANGLES, boxIndices.length, gl.UNSIGNED_SHORT, 0);

        requestAnimationFrame(loop);
    };
    requestAnimationFrame(loop);

};