precision mediump float;
attribute vec3 vertexPosition;
attribute vec2 vertexTextCoord;
varying vec2 fragTextCoord;
uniform mat4 mWorld;
uniform mat4 mView;
uniform mat4 mProj;

void main()
{
 fragTextCoord = vertexTextCoord;
 gl_Position = mProj * mView * mWorld * vec4(vertexPosition, 1.0);
}
