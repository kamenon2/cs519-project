//load image
var loadImage = function (url, callback) {
    var image = new Image();
    image.onload = function () {
      callback(null, image);
    };
    image.src = url;
};

var loadJson = function (url, callback) {
    loadText(url, function (err, result) {
       if (err) {
           callback(err);
       } else {
           try {
               callback(null, JSON.parse(result))
           } catch (ex) {
               callback(ex);
           }
       }
    });
}

//Load a json resource from a a file
var loadText = function (url, callback) {
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.onload = function () {
        if (request.status < 200 || request.status > 299) {
            callback('Error: HTTP status ' + request.status + ' on resource ' + url);
        } else {
            callback(null, request.responseText);
        }
    };
    request.send();
};