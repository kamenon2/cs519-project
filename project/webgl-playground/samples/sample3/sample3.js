var RotateCube = function () {
    console.log('Making a cube initiated ... ');
    console.log(Date.now().toString());

    var canvas = document.getElementById('sample-surface-3');
    var gl = canvas.getContext("webgl2");

    if (!gl){
        alert('Your browser does not support WebGL');
    }

    /*
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    gl.viewport(0, 0, window.innerWidth, innerHeight)
    */
    gl.clearColor(R = 0.75, G = 0.85, B = 0.8, A = 0.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT); // to paint. need to set the color buffer and depth buffer. eg: CIRCLE BEHIND SQUARE
    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.CULL_FACE);
    //Vertex shader and Fragment shader
    var vertexShader = gl.createShader(gl.VERTEX_SHADER);
    var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(vertexShader, vertexShaderText);
    gl.shaderSource(fragmentShader, fragmentShaderText);
    gl.compileShader(vertexShader);
    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
        console.error('ERROR compiling vertex shader!', gl.getShaderInfoLog(vertexShader));
        return;
    }
    console.log(vertexShader);

    gl.compileShader(fragmentShader);
    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
        console.error('ERROR compiling vertex shader!', gl.getShaderInfoLog(fragmentShader));
        return;
    }
    console.log(fragmentShader);

    var prgrm = gl.createProgram();
    gl.attachShader(prgrm, vertexShader);
    gl.attachShader(prgrm, fragmentShader);

    //link the program
    gl.linkProgram(prgrm);
    if (!gl.getProgramParameter(prgrm, gl.LINK_STATUS)) {
        console.error('ERROR linking program!', gl.getProgramInfoLog(prgrm));
        return;
    }
    gl.validateProgram(prgrm);
    if(!gl.getProgramParameter(prgrm, gl.VALIDATE_STATUS)) {
        console.error('ERROR validation program!', gl.getProgramInfoLog(prgrm))
        return;
    }

    /*
        - Vertices are the points that define the corners of 3D objects.
        - Each vertex is represented by three floating-point numbers that correspond to the x,y and x coordinates of the vertex
        - these vertices are represents in Javascript array
     */
    var boxVertices =
        [ // X, Y, Z           R, G, B
            // Top
            -1.0, 1.0, -1.0, 0.5, 0.5, 0.5,
            -1.0, 1.0, 1.0, 0.5, 0.5, 0.5,
            1.0, 1.0, 1.0, 0.5, 0.5, 0.5,
            1.0, 1.0, -1.0, 0.5, 0.5, 0.5,

            // Left
            -1.0, 1.0, 1.0, 0.75, 0.25, 0.5,
            -1.0, -1.0, 1.0, 0.75, 0.25, 0.5,
            -1.0, -1.0, -1.0, 0.75, 0.25, 0.5,
            -1.0, 1.0, -1.0, 0.75, 0.25, 0.5,

            // Right
            1.0, 1.0, 1.0, 0.25, 0.25, 0.75,
            1.0, -1.0, 1.0, 0.25, 0.25, 0.75,
            1.0, -1.0, -1.0, 0.25, 0.25, 0.75,
            1.0, 1.0, -1.0, 0.25, 0.25, 0.75,

            // Front
            1.0, 1.0, 1.0, 1.0, 0.0, 0.15,
            1.0, -1.0, 1.0, 1.0, 0.0, 0.15,
            -1.0, -1.0, 1.0, 1.0, 0.0, 0.15,
            -1.0, 1.0, 1.0, 1.0, 0.0, 0.15,

            // Back
            1.0, 1.0, -1.0, 0.0, 1.0, 0.15,
            1.0, -1.0, -1.0, 0.0, 1.0, 0.15,
            -1.0, -1.0, -1.0, 0.0, 1.0, 0.15,
            -1.0, 1.0, -1.0, 0.0, 1.0, 0.15,

            // Bottom
            -1.0, -1.0, -1.0, 0.5, 0.5, 1.0,
            -1.0, -1.0, 1.0, 0.5, 0.5, 1.0,
            1.0, -1.0, 1.0, 0.5, 0.5, 1.0,
            1.0, -1.0, -1.0, 0.5, 0.5, 1.0,
        ];

    /*
        uidlLiquidluck@2020
     */
    var boxIndices =
        [
            // Top
            0, 1, 2,
            0, 2, 3,

            // Left
            5, 4, 6,
            6, 4, 7,

            // Right
            8, 9, 10,
            8, 10, 11,

            // Front
            13, 12, 14,
            15, 14, 12,

            // Back
            16, 17, 18,
            16, 18, 19,

            // Bottom
            21, 20, 22,
            22, 20, 23
        ];

    var boxVertexBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, boxVertexBufferObject);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(boxVertices), gl.STATIC_DRAW);// CPU TO GPU MEMORY

    var boxIdxBuffObject = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, boxIdxBuffObject);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(boxIndices), gl.STATIC_DRAW);


    var positionAttributeLocation = gl.getAttribLocation(prgrm, 'vertexPosition');
    var colorAttributeLocation = gl.getAttribLocation(prgrm, 'vertexColor');

    gl.vertexAttribPointer(positionAttributeLocation,
        3,
        gl.FLOAT,
        false,
        6 * Float32Array.BYTES_PER_ELEMENT,
        0
    );

    gl.vertexAttribPointer(colorAttributeLocation,
        3,
        gl.FLOAT,
        false,
        6 * Float32Array.BYTES_PER_ELEMENT,
        3 * Float32Array.BYTES_PER_ELEMENT
    );

    gl.useProgram(prgrm);
    gl.enableVertexAttribArray(positionAttributeLocation);
    gl.enableVertexAttribArray(colorAttributeLocation);

    //location of the spaces in the GPU.
    var mWorldUniLoc = gl.getUniformLocation(prgrm, 'mWorld');
    var mViewUniLoc = gl.getUniformLocation(prgrm, 'mView');
    var mProjUniLoc = gl.getUniformLocation(prgrm, 'mProj');

    var worldMatrix = new Float32Array(16);
    var viewMatrix = new Float32Array(16);
    var projMatrix = new Float32Array(16);
    // and this is how the 3 arrays above get converted to 3 identities below.
    // .. the input gets converted automatically.
    mat4.identity(worldMatrix);
    mat4.lookAt(viewMatrix, [0, 0, -8], [0, 0, 0], [0, 1, 0]);
    mat4.perspective(projMatrix, glMatrix.toRadian(45), canvas.clientWidth / canvas.clientHeight, 0.1, 1000.0);

    //sending the matrices to the shader
    gl.uniformMatrix4fv(mWorldUniLoc, false, worldMatrix);
    gl.uniformMatrix4fv(mViewUniLoc, false, viewMatrix);
    gl.uniformMatrix4fv(mProjUniLoc, false, projMatrix);

    var angle = 0;
    var idMatrix = new Float32Array(16);
    mat4.identity(idMatrix);


    //inorder for us to rotate in all directions
    var xRotateMatrix = new Float32Array(16);
    var yRotateMatrix = new Float32Array(16);
    var loop = function () {
        angle = performance.now() / 1000 / 10 * 2 * Math.PI;

        mat4.rotate(xRotateMatrix, idMatrix, angle, [1, 0, 0])
        mat4.rotate(yRotateMatrix, idMatrix, angle / 2, [0, 1, 0])
        mat4.mul(worldMatrix, xRotateMatrix, yRotateMatrix);

        gl.uniformMatrix4fv(mWorldUniLoc, false, worldMatrix);

        gl.clearColor(0.75, 0.85, 0.8, 0.0);

        gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

        gl.drawElements(gl.TRIANGLES, boxIndices.length, gl.UNSIGNED_SHORT, 0);

        requestAnimationFrame(loop);
    };

    requestAnimationFrame(loop);


};


//Vertex shader to fragment shader
var vertexShaderText =
    [
        'precision mediump float;',
        '',
        'attribute vec3 vertexPosition;',
        'attribute vec3 vertexColor;',
        'varying vec3 fragColor;',
        'uniform mat4 mWorld;',
        'uniform mat4 mView;',
        'uniform mat4 mProj;',
        '',
        'void main()',
        '{',
        ' fragColor = vertexColor;',
        ' gl_Position = mProj * mView * mWorld * vec4(vertexPosition, 1.0);',
        '}'
    ].join('\n');

var fragmentShaderText =
    [
        'precision mediump float;',
        '',
        'varying vec3 fragColor;',
        'void main()',
        '{',
        ' gl_FragColor = vec4(fragColor, 1.0);', // frag color has 3 colors = RED, GREEN AND BLUE
        '}'
    ].join('\n');

// This is just the regular way to right the functions instead of the text format we used above
function vertexShader (vertexPosition, vertexColor) {
    return {
        fragColor: vertexColor,
        gl_position: [vertexPosition.x, vertexPosition.y, 0.0, 1.0]
    }
}