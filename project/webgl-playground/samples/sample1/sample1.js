var InitSample1 = function () {
    console.log('Testing this init sample1 function')
    console.log(Date.now().toString())

    var canvas = document.getElementById('sample1-surface')
    var gl = canvas.getContext("webgl2")
    console.log(gl) // just to view the gl contents
    console.log(canvas)

    if (!gl){
        console.log("Which world do you live in ???? ")
    }

    /*
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    gl.viewport(0, 0, window.innerWidth, innerHeight)
    */
    gl.clearColor(R = 0.75, G = 0.85, B = 0.8, A = 1.0)
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT); // to paint. need to set the color buffer and depth buffer. eg: CIRCLE BEHIND SQUARE

    //Vertex shader and Fragment shader
    var vertexShader = gl.createShader(gl.VERTEX_SHADER);
    var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(vertexShader, vertexShaderText);
    gl.shaderSource(fragmentShader, fragmentShaderText);
    gl.compileShader(vertexShader);
    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
        console.error('ERROR compiling vertex shader!', gl.getShaderInfoLog(vertexShader));
        return;
    }
    console.log(vertexShader);

    gl.compileShader(fragmentShader);
    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
        console.error('ERROR compiling vertex shader!', gl.getShaderInfoLog(fragmentShader));
        return;
    }
    console.log(fragmentShader);

    var prgrm = gl.createProgram();
    gl.attachShader(prgrm, vertexShader);
    gl.attachShader(prgrm, fragmentShader);

    //link the program
    gl.linkProgram(prgrm);
    if (!gl.getProgramParameter(prgrm, gl.LINK_STATUS)) {
        console.error('ERROR linking program!', gl.getProgramInfoLog(prgrm));
        return;
    }
    gl.validateProgram(prgrm);
    if(!gl.getProgramParameter(prgrm, gl.VALIDATE_STATUS)) {
        console.error('ERROR validation program!', gl.getProgramInfoLog(prgrm))
        return;
    }

    /*
        - Vertices are the points that define the corners of 3D objects.
        - Each vertex is represented by three floating-point numbers that correspond to the x,y and x coordinates of the vertex
        - these vertices are represents in Javascript array
     */
    var triangleVertices =
        [
            0.0, 0.5,      1.0, 1.0, 0.0,
            -0.5, -0.5,     0.7, 0.0, 1.0,
            0.5, -0.5,      0.1, 1.0, 0.6
        ];

    var triangleVertexBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, triangleVertexBufferObject);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVertices), gl.STATIC_DRAW);// CPU TO GPU MEMORY

    var positionAttributeLocation = gl.getAttribLocation(prgrm, 'vertexPosition');
    var colorAttributeLocation = gl.getAttribLocation(prgrm, 'vertexColor');
    gl.vertexAttribPointer(positionAttributeLocation,
        2,
        gl.FLOAT,
        false,
        5 * Float32Array.BYTES_PER_ELEMENT,
        0
        );

    gl.vertexAttribPointer(colorAttributeLocation,
        3,
        gl.FLOAT,
        false,
        5 * Float32Array.BYTES_PER_ELEMENT,
        2 * Float32Array.BYTES_PER_ELEMENT
    );

    gl.enableVertexAttribArray(positionAttributeLocation);
    gl.enableVertexAttribArray(colorAttributeLocation);
    gl.useProgram(prgrm);
    gl.drawArrays(gl.TRIANGLES, 0, 3);

    //Vertex shader to fragment shader
};

var vertexShaderText =
    [
        'precision mediump float;',
        '',
        'attribute vec2 vertexPosition;',
        'attribute vec3 vertexColor;',
        'varying vec3 fragColor;',
        'uniform float screenWidth;',
        '',
        'void main()',
        '{',
        ' fragColor = vertexColor;',
        ' gl_Position = vec4(vertexPosition, 0.0, 1.0);',
        '}'
    ].join('\n');

var fragmentShaderText =
    [
        'precision mediump float;',
        '',
        'varying vec3 fragColor;',
        'void main()',
        '{',
        ' gl_FragColor = vec4(fragColor, 1.0);', // frag color has 3 colors = RED, GREEN AND BLUE
        '}'
    ].join('\n');

// This is just the regular way to right the functions instead of the text format we used above
function vertexShader (vertexPosition, vertexColor) {
    return {
        fragColor: vertexColor,
        gl_position: [vertexPosition.x, vertexPosition.y, 0.0, 1.0]
    }
}